﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment10
{
    class Program
    {

        static void Main(string[] args)
        {
            var stringArrayFull = new string[5] { "Widget 15.50", "Thingy 50.99", "Ratchet25.00", "Clanger115.49", "Fracker75.25" };
            var stringArrayName = new string[5];
            var doubleArray = new double[5];

            ParseArray(stringArrayFull, stringArrayName, doubleArray);

            for (int i = 0; i < stringArrayFull.Length; i++)
            {
                Console.WriteLine($"{stringArrayName[i]} {doubleArray[i].ToString("c2")}");
            }
            Console.Read();
        }

        private static void ParseArray(string[] stringArrayFull, string[] stringArrayName, double[] doubleArray)
        {
            for (int i = 0; i < stringArrayFull.Length; i++)
            {
                stringArrayName[i] = stringArrayFull[i].Substring(0, 7);
                doubleArray[i] = double.Parse(stringArrayFull[i].Substring(7));
            }
        }
    }
}
