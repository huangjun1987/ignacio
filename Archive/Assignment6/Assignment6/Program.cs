﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6
{
    class Program
    {
        static void Main(string[] args)
        {
            CalculateHypotenuse();

            Console.Read();
        }

        private static void CalculateHypotenuse()
        {
            while (true)
            {
                Console.Write("enter length of first side: ");
                var firstSide = double.Parse(Console.ReadLine());
                Console.Write("enter length of second side: ");
                var secondSide = double.Parse(Console.ReadLine());
                if (firstSide == 0 && secondSide == 0)
                {
                    return;
                }

                var hypotenuse = Math.Sqrt(firstSide * firstSide + secondSide * secondSide);
                Console.WriteLine($"The hypotenuse is {hypotenuse}");
            }
        }
    }
}
