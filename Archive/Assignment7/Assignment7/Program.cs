﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment7
{
    class Program
    {
        static void Main(string[] args)
        {
            int randomValue = 0;
            double averageValue = 0;
            double totalValue = 0;
            double inputEntry= 0;

            for (int i = 0; i < 20; i++)
            {
                randomValue += GetRandom();
            }

            Calculate(randomValue, ref averageValue, 20);

            Console.WriteLine($"The average of the 20 random numbers is {averageValue}");

            for (int i = 0; i < 5; i++)
            {
                Console.Write("Enter a double value ");
                inputEntry = double.Parse(Console.ReadLine());
                Calculate(inputEntry, ref totalValue);
            }
            Console.WriteLine($"The total is {totalValue}");

            Console.Read();
        }
        private static void Calculate(double inputEntry, ref double totalValue)
        {
            totalValue += inputEntry;
        }
        private static void Calculate(int randomValue, ref double averageValue, int times)
        {
            averageValue = randomValue / times;
        }

        private static int GetRandom()
        {
            var random = new Random();
            return random.Next(1, 100);
        }
    }
}
