﻿using System;
using System.Text;

namespace Assignment11
{
    class Program
    {
        static void Main(string[] args)
        {
            var apartment = new int[5] { 123, 204, 601, 609, 612 };
            var rent = new int[5] { 500, 750, 495, 800, 940 };

            Console.Write("Enter the apartment number ");

            var apartmentNo = int.Parse(Console.ReadLine());

            var rentAmount = getRent(apartment, rent, apartmentNo);

            Console.WriteLine(printIt(apartmentNo,rentAmount));

            Console.Read();
        }

        private static string printIt(int apartmentNo, int rentAmount)
        {
            var sb = new StringBuilder();
            sb.Append("Rent for apartment # ");
            sb.Append(apartmentNo);
            sb.Append(" is ");
            sb.Append(rentAmount);

            return sb.ToString();
        }

        private static int getRent(int[] apartment, int[] rent, int apartmentNo)
        {
            for (int i = 0; i < 5; i++)
            {
                if (apartment[i] == apartmentNo)
                {
                    return rent[i];
                }
            }

            return 0;
        }
    }
}
