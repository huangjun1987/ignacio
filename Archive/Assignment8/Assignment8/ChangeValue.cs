﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment8
{
    public class ChangeValue
    {
        public int value1;
        public int value2;

        public ChangeValue(int val1, int val2)
        {
            if (val1 > 5)
            {
                value1 = val1;
            }
            else
            {
                value1 = val1 + val2;
            }
            if (val2 < 10)
            {
                value2 = val2 * val2 + 5;
            }
            else
            {
                value2 = val2;
            }
        }

        public int PrintIt()
        {
            return value1 * value2;
        }
    }
}
