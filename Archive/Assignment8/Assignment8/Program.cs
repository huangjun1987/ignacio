﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment8
{
    class Program
    {
        static void Main(string[] args)
        {
            int val1;
            int val2;

            Console.Write("Enter an integer value: ");
            val1 = int.Parse(Console.ReadLine());

            Console.Write("Enter a second integer value: ");
            val2 = int.Parse(Console.ReadLine());

            var changeValue = new ChangeValue(val1, val2);
            var calculatedValue = changeValue.PrintIt();
            Console.Write($"The calculated value is: {calculatedValue}");

            Console.Read();

        }
    }
}
